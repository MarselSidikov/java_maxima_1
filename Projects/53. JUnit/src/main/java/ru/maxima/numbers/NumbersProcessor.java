package ru.maxima.numbers;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 20.01.2022
 * 53. JUnit
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class NumbersProcessor {
    private final NumbersToBooleanConverter converter;

    public NumbersProcessor(NumbersToBooleanConverter converter) {
        this.converter = converter;
    }

    public List<Boolean> convertList(List<Integer> numbers) {
        return numbers
                .stream()
                .map(number -> {
                    try {
                        return converter.convert(number);
                    } catch (IllegalArgumentException e) {
                        return false;
                    }
                }).collect(Collectors.toList());
    }
}
