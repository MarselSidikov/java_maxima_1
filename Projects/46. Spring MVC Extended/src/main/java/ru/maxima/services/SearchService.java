package ru.maxima.services;

import ru.maxima.dto.AccountDto;

import java.util.List;

/**
 * 27.10.2021
 * 46. Spring MVC Extended
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface SearchService {
    List<AccountDto> searchUsersByEmail(String email);
}
