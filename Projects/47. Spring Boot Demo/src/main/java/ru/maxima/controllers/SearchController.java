package ru.maxima.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.maxima.dto.AccountDto;
import ru.maxima.services.SearchService;

import java.util.List;

/**
 * 27.10.2021
 * 46. Spring MVC Extended
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@RequiredArgsConstructor
@Controller
@RequestMapping("/search")
public class SearchController {

    private final SearchService searchService;

    @GetMapping
    public String getSearchPage() {
        return "search";
    }

    @GetMapping(value = "/users")
    @ResponseBody
    public ResponseEntity<?> searchUsers(@RequestParam("email") String email) {
        return ResponseEntity.ok(searchService.searchUsersByEmail(email));
    }

    @GetMapping(value = "/cars")
    @ResponseBody
    public ResponseEntity<?> searchCars(@RequestParam("color") String color) {
        return ResponseEntity.ok(searchService.searchCarsByColor(color));
    }
}
