package ru.maxima.services;

import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

/**
 * 15.11.2021
 * Spring Boot Demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface FilesService {
    void saveFile(MultipartFile file, String description);

    void addFileToResponse(String fileName, HttpServletResponse response);

}
