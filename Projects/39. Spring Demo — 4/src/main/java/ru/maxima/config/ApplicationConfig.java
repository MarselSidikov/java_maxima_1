package ru.maxima.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import ru.maxima.SignUpService;
import ru.maxima.blacklist.PasswordBlackList;
import ru.maxima.blacklist.PasswordBlackListFileImpl;
import ru.maxima.blacklist.PasswordBlackListHardcodeImpl;
import ru.maxima.repositories.AccountsRepository;
import ru.maxima.repositories.AccountsRepositoryNamedParameterJdbcTemplateImpl;
import ru.maxima.validators.SignUpDataValidator;
import ru.maxima.validators.SignUpDataValidatorEmailAndPasswordImpl;
import ru.maxima.validators.email.EmailValidator;
import ru.maxima.validators.email.EmailValidatorRegexImpl;
import ru.maxima.validators.password.PasswordByLengthValidator;
import ru.maxima.validators.password.PasswordCharactersValidatorImpl;
import ru.maxima.validators.password.PasswordValidator;

import javax.sql.DataSource;

/**
 * 09.09.2021
 * 36. Spring Demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@PropertySource(value = "classpath:application.properties")
@ComponentScan(value = "ru.maxima")
@Component
public class ApplicationConfig {

    private final Environment environment;

    @Autowired
    public ApplicationConfig(Environment environment) {
        this.environment = environment;
    }

    @Bean
    public DataSource dataSource(HikariConfig hikariConfig) {
        return new HikariDataSource(hikariConfig);
    }

    @Bean
    public HikariConfig hikariConfig() {
        HikariConfig config = new HikariConfig();
        config.setJdbcUrl(environment.getProperty("db.url"));
        config.setDriverClassName(environment.getProperty("db.driverClassName"));
        config.setUsername(environment.getProperty("db.user"));
        config.setPassword(environment.getProperty("db.password"));
        config.setMaximumPoolSize(environment.getProperty("db.hikari.max-pool-size", Integer.class));
        return config;
    }
}
