package ru.maxima.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;
import ru.maxima.dto.UserDto;

/**
 * 20.12.2021
 * 51. Redis
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
public class UsersController {

    private final RedisTemplate<String, String> redisTemplate;

    @PostMapping
    public void addUser(@RequestParam("email") String email, @RequestParam("password") String password, @RequestParam("age") Integer age) {
        redisTemplate.opsForHash().put(email, "password", password);
        redisTemplate.opsForHash().put(email, "age", age.toString());
    }

    @GetMapping
    public UserDto getUserByEmail(@RequestParam("email") String email) {
        String password = (String) redisTemplate.opsForHash().get(email, "password");
        Integer age = Integer.parseInt((String) redisTemplate.opsForHash().get(email, "age"));
        return UserDto.builder()
                .password(password)
                .age(age)
                .build();
    }
}
