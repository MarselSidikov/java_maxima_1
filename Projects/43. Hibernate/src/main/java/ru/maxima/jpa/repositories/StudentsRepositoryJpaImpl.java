package ru.maxima.jpa.repositories;


import ru.maxima.jpa.models.Student;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.List;

/**
 * 13.10.2021
 * 43. Hibernate
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class StudentsRepositoryJpaImpl implements StudentsRepository {

    private final EntityManager entityManager;

    public StudentsRepositoryJpaImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void save(Student student) {
        // создаем транзакцию
        EntityTransaction transaction = entityManager.getTransaction();
        // начать транзакцию
        transaction.begin();
        // внутри нее выполняем операцию
        entityManager.persist(student);
        // завершаем транзакцию
        transaction.commit();
    }
}
