package ru.maxima.spring.repositories;

import org.springframework.stereotype.Repository;
import ru.maxima.spring.models.Course;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * 13.10.2021
 * 43. Hibernate
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Repository
public class CoursesRepositoryJpaImpl implements CoursesRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void save(Course course) {
        // создаем транзакцию
        EntityTransaction transaction = entityManager.getTransaction();
        // начать транзакцию
        transaction.begin();
        // внутри нее выполняем операцию
        entityManager.persist(course);
        // завершаем транзакцию
        transaction.commit();
    }

    @Override
    public List<Course> findAllByLesson_name(String name) {
        TypedQuery<Course> query = entityManager.createQuery("select course from Course course left join course.lessons lesson where lesson.name = :name", Course.class);
        query.setParameter("name", name);
        return query.getResultList();
    }
}
