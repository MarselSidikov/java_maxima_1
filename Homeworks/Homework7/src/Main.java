import java.util.Arrays;

public class Main {

    public static void replace(int numbers[]) {
        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] % 2 == 0) {
                numbers[i] = 0;
            }
        }
    }

    public static void main(String[] args) {
        int[] array = new int[5];

        array[0] = 10;
        array[1] = 13;
        array[2] = 10;
        array[3] = 13;
        array[4] = 10;

        replace(array);
        System.out.println(Arrays.toString(array));
    }
}
