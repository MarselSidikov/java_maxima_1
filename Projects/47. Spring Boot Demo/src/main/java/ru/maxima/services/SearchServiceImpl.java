package ru.maxima.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.maxima.dto.AccountDto;
import ru.maxima.dto.CarDto;
import ru.maxima.repositories.AccountsRepository;
import ru.maxima.repositories.CarsRepository;

import java.util.List;

import static ru.maxima.dto.AccountDto.from;
import static ru.maxima.dto.CarDto.from;

/**
 * 27.10.2021
 * 46. Spring MVC Extended
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Service
@RequiredArgsConstructor
public class SearchServiceImpl implements SearchService {

    private final AccountsRepository accountsRepository;
    private final CarsRepository carsRepository;

    @Override
    public List<AccountDto> searchUsersByEmail(String email) {
        return from(accountsRepository.findByEmailLike("%" + email + "%"));
    }

    @Override
    public List<CarDto> searchCarsByColor(String color) {
        return from(carsRepository.findByColorLike("%" + color + "%"));
    }
}
