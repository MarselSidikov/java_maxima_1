package ru.maxima.hibernate.app;

import org.hibernate.query.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import ru.maxima.hibernate.models.OldStudent;

/**
 * 07.10.2021
 * 43. Hibernate
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MainQuery {
    public static void main(String[] args) {
        Configuration configuration = new Configuration();
        configuration.configure("hibernate\\hibernate.cfg.xml");

        SessionFactory sessionFactory = configuration.buildSessionFactory();

        Session session = sessionFactory.openSession();

        Query<OldStudent> query = session.createQuery("select s from Student s where " +
                "s.lastName = 'Сидиков'", OldStudent.class);

        OldStudent student = query.getSingleResult();
        int i = 0;
        session.close();
    }
}
