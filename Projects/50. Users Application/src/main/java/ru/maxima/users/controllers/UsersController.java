package ru.maxima.users.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.maxima.users.models.User;
import ru.maxima.users.repositories.UsersRepository;

import java.util.List;

/**
 * 16.12.2021
 * 50. Users Application
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/users")
public class UsersController {

    private final UsersRepository usersRepository;

    @GetMapping
    public List<User> getUsers() {
        return usersRepository.findAll();
    }

    @PostMapping
    public User addUser(User user) {
        return usersRepository.save(user);
    }
}
