package ru.maxima.services;

import ru.maxima.dto.AccountDto;

import java.util.List;

/**
 * 20.09.2021
 * 41. Java Servlet Example
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface UsersService {
    List<AccountDto> getAllUsers();
}
