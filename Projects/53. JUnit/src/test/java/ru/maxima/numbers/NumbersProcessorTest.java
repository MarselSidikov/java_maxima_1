package ru.maxima.numbers;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.maxima.numbers.NumbersProcessor;
import ru.maxima.numbers.NumbersToBooleanConverter;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;

/**
 * 20.01.2022
 * 53. JUnit
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@DisplayNameGeneration(value = DisplayNameGenerator.ReplaceUnderscores.class)
@DisplayName("NumbersProcessor is working when")
@ExtendWith(MockitoExtension.class)
class NumbersProcessorTest {

    private NumbersProcessor processor;

    private static final List<Integer> NUMBERS = Arrays.asList(0, 1, 3, 4, 8, 11);

    private static final List<Boolean> EXPECTED = Arrays.asList(false, false, false, true, true, false);

    @Mock
    private NumbersToBooleanConverter converter; // мок-объект - не содержит реальной реализации

    @BeforeEach
    public void setUp() {
        // stubbing
        when(converter.convert(0)).thenThrow(IllegalArgumentException.class);
        when(converter.convert(1)).thenThrow(IllegalArgumentException.class);
        lenient().when(converter.convert(3)).thenReturn(false);
        lenient().when(converter.convert(4)).thenReturn(true);
        lenient().when(converter.convert(8)).thenReturn(true);
        when(converter.convert(11)).thenReturn(false);
        this.processor = new NumbersProcessor(converter);
    }

    @Test
    public void smoke() {
        assertThat(converter, is(notNullValue()));
        assertThat(converter.convert(11), is(false));
    }

    @Test
    public  void testConvertList() {
        assertEquals(processor.convertList(NUMBERS), EXPECTED);
    }
}