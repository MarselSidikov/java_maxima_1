package ru.maxima.services;

import ru.maxima.dto.SignUpForm;

/**
 * 27.10.2021
 * 46. Spring MVC Extended
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface SignUpService {
    void signUp(SignUpForm form);
}
