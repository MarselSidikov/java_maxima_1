package ru.maxima.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.maxima.dto.CarDto;
import ru.maxima.services.UsersService;

/**
 * 01.11.2021
 * Spring Boot Demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Controller
@RequiredArgsConstructor
@RequestMapping("/users")
public class UsersController {

    private final UsersService usersService;

    @GetMapping
    public String getUsers(Model model) {
        model.addAttribute("accounts", usersService.getAllUsers());
        return "users";
    }

    @PostMapping("/{user-id}/delete")
    public String deleteUser(@PathVariable("user-id") Long userId) {
        usersService.deleteUser(userId);
        return "redirect:/users";
    }

    @GetMapping("/{user-id}/cars")
    public String getCarsOfUserPage(@PathVariable("user-id") Long userId, Model model) {
        model.addAttribute("cars", usersService.getCarsByUser(userId));
        model.addAttribute("carsWithoutUser", usersService.getCarsWithoutUser());
        return "cars_of_user";
    }

    @PostMapping(value = "/{user-id}/cars")
    public String addCarToUser(@PathVariable("user-id") Long userId, CarDto car) {
        usersService.addCarToUser(userId, car);
        return "redirect:/users/" + userId + "/cars";
    }
}
