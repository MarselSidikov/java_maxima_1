package ru.maxima;

/**
 * 18.06.2021
 * 11. Static Members
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main2 {
    public static void main(String[] args) {
        SomeClass.staticMethod();
    }
}
