# REDIS

REDIS - in memory no sql database. MAP в виде отдельного СУБД, гарантирует очень высокую скорость работы.

## Команды REDIS

```
flushall

set user marsel

get user

exists user

exists user1

flushall

HSET user name marsel

HSET user age 27

HGET user name

GET user age

HGETALL user

HVALS user

HKEYS user

SADD passwords qwerty007 qwerty008 qwerty009

SMEMBERS passwords

SCARD passwords

LPUSH marsel 27 

RPUSH marsel 29

LPANGE marsel 0 3
```

## Docker REDIS

* Скачать образ REDIS

```
docker pull redis
```

* Запустить контейнер

```
docker run --name redis-container -v redis-volume:/data -p 6380:6379 -d redis-image-with-persistence
```

* Рекомендую Redis Explorer