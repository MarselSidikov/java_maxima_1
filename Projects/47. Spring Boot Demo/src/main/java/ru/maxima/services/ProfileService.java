package ru.maxima.services;

import ru.maxima.dto.AccountDto;

/**
 * 26.11.2021
 * Spring Boot Demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface ProfileService {
    AccountDto getUser(Long currentUserId);
}
