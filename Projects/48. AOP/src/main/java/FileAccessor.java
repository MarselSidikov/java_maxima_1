import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.time.LocalDateTime;

/**
 * 04.12.2021
 * 48. AOP
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class FileAccessor {
    private LocalDateTime lastFileOpenTime;

    public InputStream openFile(String fileName) {
        try {
            lastFileOpenTime = LocalDateTime.now();
            return new FileInputStream(fileName);
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException();
        }
    }

    public LocalDateTime getLastFileOpenTime() {
        return lastFileOpenTime;
    }
}
