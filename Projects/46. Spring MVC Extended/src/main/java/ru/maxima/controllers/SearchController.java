package ru.maxima.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.maxima.dto.AccountDto;
import ru.maxima.services.SearchService;
import ru.maxima.services.UsersService;

import java.util.List;

/**
 * 27.10.2021
 * 46. Spring MVC Extended
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Controller
public class SearchController {

    @Autowired
    private SearchService searchService;

    @RequestMapping(value = "/search")
    public String getSearchPage() {
        return "search";
    }

    @RequestMapping(value = "/users/search")
    @ResponseBody
    public List<AccountDto> searchUsers(@RequestParam("email") String email) {
        return searchService.searchUsersByEmail(email);
    }
}
