package ru.maxima.jpa.app;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import ru.maxima.jpa.models.Course;
import ru.maxima.jpa.models.Lesson;
import ru.maxima.jpa.models.Student;
import ru.maxima.jpa.repositories.*;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * 13.10.2021
 * 43. Hibernate
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Application {
    public static void main(String[] args) {
        Configuration configuration = new Configuration();
        configuration.configure("hibernate\\hibernate.cfg.xml");

        SessionFactory sessionFactory = configuration.buildSessionFactory();

        EntityManager entityManager = sessionFactory.createEntityManager();

        CoursesRepository coursesRepository = new CoursesRepositoryJpaImpl(entityManager);
        StudentsRepository studentsRepository = new StudentsRepositoryJpaImpl(entityManager);
        LessonsRepository lessonsRepository = new LessonsRepositoryJpaImpl(entityManager);

        Course java = Course.builder()
                .title("Java")
                .build();

        Course sql = Course.builder()
                .title("SQL")
                .build();

        coursesRepository.save(java);
        coursesRepository.save(sql);

        // создал урок, указал у урока уже сохраненный курс
        Lesson javaCore = Lesson.builder()
                .course(java)
                .name("Java core")
                .build();

        // создал урок, указал у урока уже сохраненный курс
        Lesson spring = Lesson.builder()
                .course(java)
                .name("Spring")
                .build();

        // создал урок, указал у урока уже сохраненный курс
        Lesson selects = Lesson.builder()
                .course(sql)
                .name("Select")
                .build();

        // создал урок, указал у урока уже сохраненный курс
        Lesson inserts = Lesson.builder()
                .course(sql)
                .name("Insert")
                .build();

        // сохранили уроки
        lessonsRepository.save(javaCore);
        lessonsRepository.save(spring);
        lessonsRepository.save(selects);
        lessonsRepository.save(inserts);

        // создали студентов
        Student student1 = Student.builder()
                .firstName("Марсель")
                .lastName("Сидиков")
                .courses(new ArrayList<>())
                .build();

        Student student2 = Student.builder()
                .firstName("Айрат")
                .lastName("Мухутдинов")
                .courses(new ArrayList<>())
                .build();

        // сохранили студентов
        studentsRepository.save(student1);
        studentsRepository.save(student2);
        entityManager.close();
        student1.setCourses(Arrays.asList(java, sql));
        studentsRepository.save(student1);
        // Достает из кэша, а не из базы
        Course course = coursesRepository.findAllByLesson_name("Select").get(0);
    }
}
