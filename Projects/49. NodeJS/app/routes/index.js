const usersRoutes = require('./users_routes');

module.exports = function (app, bodyParser, fileStorage) {
    usersRoutes(app, bodyParser, fileStorage);
}