package ru.maxima.numbers;

import java.util.Arrays;
import java.util.List;

/**
 * 17.01.2022
 * 53. JUnit
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        NumberUtil numberUtil = new NumberUtil();
        NumbersProcessor processor = new NumbersProcessor(numberUtil);

        List<Integer> numbers = Arrays.asList(1, 5, 10, 127, 169, 113);
        List<Boolean> booleans = processor.convertList(numbers);
        System.out.println(booleans);

    }
}
