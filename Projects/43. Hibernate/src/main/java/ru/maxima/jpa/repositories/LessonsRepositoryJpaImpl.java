package ru.maxima.jpa.repositories;

import ru.maxima.jpa.models.Course;
import ru.maxima.jpa.models.Lesson;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

/**
 * 13.10.2021
 * 43. Hibernate
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class LessonsRepositoryJpaImpl implements LessonsRepository {

    private final EntityManager entityManager;

    public LessonsRepositoryJpaImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void save(Lesson lesson) {
        // создаем транзакцию
        EntityTransaction transaction = entityManager.getTransaction();
        // начать транзакцию
        transaction.begin();
        // внутри нее выполняем операцию
        entityManager.persist(lesson);
        // завершаем транзакцию
        transaction.commit();
    }
}
