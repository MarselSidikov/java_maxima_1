package ru.maxima.spring.app;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.maxima.spring.config.JpaConfig;
import ru.maxima.spring.repositories.StudentsRepository;

/**
 * 14.10.2021
 * 43. Hibernate
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(JpaConfig.class);
        StudentsRepository studentsRepository = context.getBean(StudentsRepository.class);
        System.out.println(studentsRepository.findAll());
    }
}
