package ru.maxima.users.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.maxima.users.models.User;

/**
 * 16.12.2021
 * 50. Users Application
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */

public interface UsersRepository extends JpaRepository<User, Long> {
}
