package ru.maxima.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.maxima.dto.CarDto;
import ru.maxima.models.Car;
import ru.maxima.repositories.CarsRepository;

import java.util.List;

import static ru.maxima.dto.CarDto.from;

/**
 * 27.10.2021
 * 46. Spring MVC Extended
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Service
@RequiredArgsConstructor
public class CarsServiceImpl implements CarsService {

    private final CarsRepository carsRepository;

    @Override
    public void addCar(CarDto car) {
        Car newCar = Car.builder()
                .color(car.getColor())
                .model(car.getModel())
                .build();

        carsRepository.save(newCar);
    }

    @Override
    public List<CarDto> getAllCars() {
        return from(carsRepository.findAll());
    }
}
