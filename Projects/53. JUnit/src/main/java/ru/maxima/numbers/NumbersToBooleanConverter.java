package ru.maxima.numbers;

/**
 * 20.01.2022
 * 53. JUnit
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface NumbersToBooleanConverter {
    boolean convert(int number);
}
