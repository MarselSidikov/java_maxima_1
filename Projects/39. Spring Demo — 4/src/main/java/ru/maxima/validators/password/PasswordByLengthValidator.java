package ru.maxima.validators.password;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 06.09.2021
 * 36. Spring Demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Component(value = "passwordValidatorByLength")
public class PasswordByLengthValidator implements PasswordValidator {

    private int minLength;

    @Value(value = "${passwordValidatorByLength.min-length}")
    public void setMinLength(int minLength) {
        this.minLength = minLength;
    }

    @Override
    public boolean isValid(String password) {
        if (!(password.length() >= minLength)) {
            System.err.println("Длина пароля не соответствует требованиям!");
            return false;
        } else return true;
    }
}
