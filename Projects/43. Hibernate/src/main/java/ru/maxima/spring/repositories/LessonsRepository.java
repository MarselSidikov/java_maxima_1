package ru.maxima.spring.repositories;

import ru.maxima.spring.models.Lesson;

/**
 * 13.10.2021
 * 43. Hibernate
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface LessonsRepository {
    void save(Lesson lesson);
}
