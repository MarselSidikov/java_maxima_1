package ru.maxima.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.maxima.dto.SignUpForm;
import ru.maxima.services.SignUpService;

/**
 * 27.10.2021
 * 46. Spring MVC Extended
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Controller
public class SignUpController {

    @Autowired
    private SignUpService signUpService;

    @RequestMapping("/signUp")
    public String getSignUpPage() {
        return "sign_up";
    }

    @RequestMapping(value = "/signUp", method = RequestMethod.POST)
    public String signUp(SignUpForm form) {
        signUpService.signUp(form);
        return "redirect:/signIn";
    }
}
