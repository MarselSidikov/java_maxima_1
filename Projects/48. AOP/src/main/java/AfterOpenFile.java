import org.springframework.aop.AfterReturningAdvice;

import java.io.InputStream;
import java.lang.reflect.Method;

/**
 * 04.12.2021
 * 48. AOP
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class AfterOpenFile implements AfterReturningAdvice {

    @Override
    public void afterReturning(Object returnValue, Method method, Object[] args, Object target) throws Throwable {
        System.out.println("Был вызван метод - " + method.getName());
        System.out.println("Открыт InputStream, доступно байт -" + ((InputStream)returnValue).available());
        System.out.println(((FileAccessor)target).getLastFileOpenTime());
    }
}
