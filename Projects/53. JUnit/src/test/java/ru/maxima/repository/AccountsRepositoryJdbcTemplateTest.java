package ru.maxima.repository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import javax.sql.DataSource;

/**
 * 20.01.2022
 * 53. JUnit
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
class AccountsRepositoryJdbcTemplateTest {

    private AccountsRepositoryJdbcTemplateImpl accountsRepository;

    @BeforeEach
    void setUp() {
        DataSource dataSource = new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.H2)
                .addScript("schema.sql")
                .addScript("data.sql")
                .build();

        accountsRepository = new AccountsRepositoryJdbcTemplateImpl(dataSource);
    }

    @Test
    void findAll() {
        System.out.println(accountsRepository.findAll());
    }
}