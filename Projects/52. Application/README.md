## Запуск Redis

```
 docker run --name redis-container -v redis-volume:/data -p 6380:6379 -d redis-image-with-persistence
```

## Запуск PostgreSQL

```
docker run --name postgresql-container -p 5433:5432 -e POSTGRES_PASSWORD=qwerty009 -e POSTGRES_DB=app_db -v pgdata:/var/lib/postgresql/data -d postgres
```

## Запуск приложения

```
docker run --name taxi-app-container 
	-e SPRING_DATASOURCE_USERNAME=postgres 
	-e SPRING_DATASOURCE_PASSWORD=qwerty009 
	-e SPRING_DATASOURCE_URL=jdbc:postgresql://172.17.0.3:5432/app_db 
	-e SPRING_REDIS_HOST=172.17.0.2 
	-e SPRING_REDIS_PORT=6379 
	-p 80:8080 
	-d taxi-app-image
```