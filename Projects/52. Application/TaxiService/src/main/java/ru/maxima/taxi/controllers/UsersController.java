package ru.maxima.taxi.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.maxima.taxi.models.User;
import ru.maxima.taxi.repositories.UsersRepository;

import java.util.List;

/**
 * 16.12.2021
 * 50. Users Application
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/users")
public class UsersController {

    private final UsersRepository usersRepository;

    @GetMapping
    public List<User> getUsers() {
        return usersRepository.findAll();
    }

    @PostMapping
    public User addUser(@RequestBody User user) {
        return usersRepository.save(user);
    }
}
