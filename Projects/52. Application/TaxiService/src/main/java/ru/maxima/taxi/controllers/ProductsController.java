package ru.maxima.taxi.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.maxima.taxi.models.Product;
import ru.maxima.taxi.repositories.ProductsRedisRepository;

import java.util.ArrayList;
import java.util.List;

/**
 * 20.12.2021
 * 51. Redis
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/products")
public class ProductsController {

    private final ProductsRedisRepository productsRedisRepository;

    @PostMapping
    public Product addProduct(@RequestBody Product product) {
        return productsRedisRepository.save(product);
    }

    @GetMapping
    public List<Product> getProducts() {
        List<Product> products = new ArrayList<>();
        productsRedisRepository.findAll().forEach(products::add);
        return products;
    }
}
