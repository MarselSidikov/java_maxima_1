package ru.maxima.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * 30.09.2021
 * 42. Java Servlet JSP
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@WebFilter("/*")
public class AuthenticationFilter implements Filter {

    private static final String IS_AUTHENTICATED_ATTRIBUTE_NAME = "isAuthenticated";

    private static final List<String> PROTECTED_URLS = Arrays.asList(
            "/users",
            "/searchByUsers",
            "/cars",
            "/profile");

    private static final List<String> NOT_ACCESSED_FOR_AUTHENTICATED_URLS = Arrays.asList(
            "/signIn",
            "/signUp"
    );

    private static final String DEFAULT_REDIRECT_URL = "/profile";
    private static final String DEFAULT_SIGN_IN_URL = "/signIn";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        // если запрос нуждается в защите
        if (isProtected(request.getRequestURI())) {
            // запрос аутентифицирован
            if (isAuthenticated(request)) {
                // пропускаем его дальше
                chain.doFilter(request, response);
                return;
            } else {
                // если не аутентифицирован
                // заставляем заново перейти на страницу signIn
                response.sendRedirect(DEFAULT_SIGN_IN_URL);
                // останавливает работу фильтра
                return;
            }
        }
        // но страница не доступна для аутентифицированного пользователя
        if (isAuthenticated(request) && isNotAccessedForAuthenticated(request.getRequestURI())) {
            // отправляем его на профиль
            response.sendRedirect(DEFAULT_REDIRECT_URL);
            // завершаем обработку на сервере
            return;
        }
        chain.doFilter(request, response);
    }

    private boolean isProtected(String url) {
        return PROTECTED_URLS.contains(url);
    }

    private boolean isNotAccessedForAuthenticated(String url) {
        return NOT_ACCESSED_FOR_AUTHENTICATED_URLS.contains(url);
    }

    private boolean isAuthenticated(HttpServletRequest request) {
        // запрашиваем сессию текущего запроса
        // при этом, если сессии не было вообще - то вернется null, новая создаваться не будет
        // потому что мы указали create = false
        HttpSession session = request.getSession(false);
        // если сессии не было - то сразу - false
        if (session == null) {
            return false;
        }

        Boolean result = (Boolean) session.getAttribute(IS_AUTHENTICATED_ATTRIBUTE_NAME);
        return result != null && result;
    }


    @Override
    public void destroy() {

    }
}
