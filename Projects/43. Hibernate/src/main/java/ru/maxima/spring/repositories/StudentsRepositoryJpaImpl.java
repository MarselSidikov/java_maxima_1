package ru.maxima.spring.repositories;

import org.springframework.stereotype.Repository;
import ru.maxima.spring.models.Student;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * 13.10.2021
 * 43. Hibernate
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Repository
public class StudentsRepositoryJpaImpl implements StudentsRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void save(Student student) {
        // создаем транзакцию
        EntityTransaction transaction = entityManager.getTransaction();
        // начать транзакцию
        transaction.begin();
        // внутри нее выполняем операцию
        entityManager.persist(student);
        // завершаем транзакцию
        transaction.commit();
    }

    @Override
    public List<Student> findAll() {
        TypedQuery<Student> query = entityManager.createQuery("select student from Student student", Student.class);
        return query.getResultList();
    }
}
