package ru.maxima.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.maxima.dto.AccountDto;
import ru.maxima.repositories.AccountsRepository;

import java.util.List;

import static ru.maxima.dto.AccountDto.from;

/**
 * 06.09.2021
 * 36. Spring Demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Service
public class UsersServiceImpl implements UsersService {

    private final AccountsRepository accountsRepository;

    @Autowired
    public UsersServiceImpl(AccountsRepository accountsRepository) {
        this.accountsRepository = accountsRepository;
    }

    @Override
    public List<AccountDto> getAllUsers() {
        return from(accountsRepository.findAll());
    }
}
