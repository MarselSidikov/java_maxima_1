<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<script>
    function addCar(color, model) {
        let body = {
            "color": color,
            "model": model
        };

        let request = new XMLHttpRequest();

        request.open('POST', '/cars', false);
        request.setRequestHeader("Content-Type", "application/json");
        request.send(JSON.stringify(body));

        if (request.status !== 201) {
            alert("Ошибка!")
        }

    }
</script>
<body>
<h1 style="color: ${color}">Cars page</h1>
<div>
    <label for="color">Color:</label>
    <input id="color" name="color" placeholder="Example: Red"/>
    <label for="model">Model:</label>
    <input id="model" name="model" placeholder="Example: KIA">
    <button onclick="addCar(
        document.getElementById('color').value,
        document.getElementById('model').value)">Add
    </button>
</div>
</body>
</html>
