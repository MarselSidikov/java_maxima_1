package ru.maxima.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ru.maxima.security.details.UserDetailsImpl;
import ru.maxima.services.ProfileService;

/**
 * 26.11.2021
 * Spring Boot Demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@RequiredArgsConstructor
@Controller
public class ProfileController {

    private final ProfileService profileService;

    @GetMapping("/profile")
    public String getProfilePage(@AuthenticationPrincipal(expression = "id") Long currentUserId, Model model) {
        model.addAttribute("user", profileService.getUser(currentUserId));
        return "profile";
    }
}
