package ru.maxima.dto;

import lombok.Data;

/**
 * 27.09.2021
 * 42. Java Servlet JSP
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Data
public class CarDto {
    private String model;
    private String color;
}
