package ru.maxima.repository;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * 20.01.2022
 * 53. JUnit
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class AccountsRepositoryJdbcTemplateImpl {
    private static final String SQL_SELECT_ALL = "select id, name from account";

    private final JdbcTemplate jdbcTemplate;

    public AccountsRepositoryJdbcTemplateImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    private static final RowMapper<Account> accountRowMapper =
            (row, rowNumber) -> new Account(row.getLong("id"), row.getString("name"));

    public List<Account> findAll() {
        return jdbcTemplate.query(SQL_SELECT_ALL, accountRowMapper);
    }
}

