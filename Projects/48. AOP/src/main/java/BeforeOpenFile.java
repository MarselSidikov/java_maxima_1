import org.springframework.aop.MethodBeforeAdvice;

import java.lang.reflect.Method;

/**
 * 04.12.2021
 * 48. AOP
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class BeforeOpenFile implements MethodBeforeAdvice {
    @Override
    public void before(Method method, Object[] args, Object target) throws Throwable {
        System.out.println("Открываем файл " + args[0]);
    }
}
