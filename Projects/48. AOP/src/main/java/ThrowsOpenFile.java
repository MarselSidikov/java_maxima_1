import org.springframework.aop.ThrowsAdvice;

import java.io.FileNotFoundException;

/**
 * 04.12.2021
 * 48. AOP
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class ThrowsOpenFile implements ThrowsAdvice {

    public void afterThrowing(Exception exception) {
        if (exception instanceof IllegalArgumentException) {
            System.out.println("Файл не найден!");
//            throw new IllegalArgumentException(exception);
        }
    }
}
