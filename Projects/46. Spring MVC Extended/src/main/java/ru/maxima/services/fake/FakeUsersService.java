package ru.maxima.services.fake;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import ru.maxima.dto.AccountDto;
import ru.maxima.dto.CarDto;
import ru.maxima.services.UsersService;

import java.util.Arrays;
import java.util.List;

/**
 * 01.11.2021
 * 46. Spring MVC Extended
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
//@Profile("dev")
//@Service
public class FakeUsersService implements UsersService {
    @Override
    public List<AccountDto> getAllUsers() {
        return Arrays.asList(
                AccountDto.builder().email("fake0").id(0L).build(),
                AccountDto.builder().email("fake1").id(1L).build(),
                AccountDto.builder().email("fake2").id(2L).build());
    }

    @Override
    public List<CarDto> getCarsByUser(Long userId) {
        return null;
    }

    @Override
    public void addCarToUser(Long userId, CarDto car) {

    }

    @Override
    public List<CarDto> getCarsWithoutUser() {
        return null;
    }
}
