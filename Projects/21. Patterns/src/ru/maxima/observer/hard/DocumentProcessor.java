package ru.maxima.observer.hard;

/**
 * 06.07.2021
 * 21. Patterns
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// Observer
public interface DocumentProcessor {
    void handleDocument(String document);
}
