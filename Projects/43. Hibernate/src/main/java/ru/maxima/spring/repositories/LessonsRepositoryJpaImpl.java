package ru.maxima.spring.repositories;

import org.springframework.stereotype.Repository;
import ru.maxima.spring.models.Lesson;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceContext;

/**
 * 13.10.2021
 * 43. Hibernate
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Repository
public class LessonsRepositoryJpaImpl implements LessonsRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void save(Lesson lesson) {
        // создаем транзакцию
        EntityTransaction transaction = entityManager.getTransaction();
        // начать транзакцию
        transaction.begin();
        // внутри нее выполняем операцию
        entityManager.persist(lesson);
        // завершаем транзакцию
        transaction.commit();
    }
}
