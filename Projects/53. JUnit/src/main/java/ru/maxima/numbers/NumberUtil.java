package ru.maxima.numbers;

/**
 * 17.01.2022
 * 53. JUnit
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class NumberUtil implements NumbersToBooleanConverter {
    // 17, 31, 73
    public boolean isPrime(int number) {

        if (number == 0 || number == 1) {
            throw new IllegalArgumentException();
        }

        if (number == 2 || number == 3) {
            return true;
        }

        // 31 -> 31 / 2, 31 / 3, 31 / 4, 31 / 5, ... 31 / 30
        // 31 -> 31 / 2, 31 / 3, ..., 31 / 15
        // если у числа нет делителей, которые меньше или равны корню из этого числа, то дальше делителей точно нет
        // 31 -> 31 / 2, 31 / 3, 31 /4, 31 / 5 -> i <= sqrt(number) ~ i * i <= number
        for (int i = 2; i * i <= number; i++) {
            if (number % i == 0) {
                return false;
            }
        }

        return true;
    }

    // 9, 12 -> 3
    public int gcd(int a, int b) {
        while (b != 0) {
            int temp = a % b;
            a = b;
            b = temp;
        }

        return a;
    }

    @Override
    public boolean convert(int number) {
        return isPrime(number);
    }
}
