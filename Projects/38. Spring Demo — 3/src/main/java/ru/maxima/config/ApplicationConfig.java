package ru.maxima.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import ru.maxima.SignUpService;
import ru.maxima.blacklist.PasswordBlackList;
import ru.maxima.blacklist.PasswordBlackListFileImpl;
import ru.maxima.blacklist.PasswordBlackListHardcodeImpl;
import ru.maxima.repositories.AccountsRepository;
import ru.maxima.repositories.AccountsRepositoryNamedParameterJdbcTemplateImpl;
import ru.maxima.validators.SignUpDataValidator;
import ru.maxima.validators.SignUpDataValidatorEmailAndPasswordImpl;
import ru.maxima.validators.email.EmailValidator;
import ru.maxima.validators.email.EmailValidatorRegexImpl;
import ru.maxima.validators.password.PasswordByLengthValidator;
import ru.maxima.validators.password.PasswordCharactersValidatorImpl;
import ru.maxima.validators.password.PasswordValidator;

import javax.sql.DataSource;

/**
 * 09.09.2021
 * 36. Spring Demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@PropertySource(value = "classpath:application.properties")
public class ApplicationConfig {

    private final Environment environment;

    @Autowired
    public ApplicationConfig(Environment environment) {
        this.environment = environment;
    }

    @Bean
    public SignUpService signUpService() {
        return new SignUpService(accountsRepository(), passwordBlackListFile(), signUpDataValidator());
    }

    @Bean
    public PasswordBlackList passwordBlackListFile() {
        return new PasswordBlackListFileImpl(environment.getProperty("passwordBlackListFile.fileName"));

    }
    @Bean
    public PasswordBlackList passwordBlackListHardcode() {
        return new PasswordBlackListHardcodeImpl();
    }

    @Bean
    public SignUpDataValidator signUpDataValidator() {
        return new SignUpDataValidatorEmailAndPasswordImpl(passwordValidatorByCharacters(), emailValidatorRegex());
    }
    @Bean
    public EmailValidator emailValidatorRegex() {
        EmailValidatorRegexImpl emailValidatorRegex = new EmailValidatorRegexImpl();
        emailValidatorRegex.setRegex(environment.getProperty("emailValidatorRegex.regex"));
        return emailValidatorRegex;
    }

    @Bean
    public PasswordValidator passwordValidatorByCharacters() {
        return new PasswordCharactersValidatorImpl();
    }

    @Bean
    public PasswordValidator passwordValidatorByLength() {
        PasswordByLengthValidator validator = new  PasswordByLengthValidator();
        validator.setMinLength(environment.getProperty("passwordValidatorByLength.min-length", Integer.class));
        return validator;
    }

    @Bean
    public AccountsRepository accountsRepository() {
        return new AccountsRepositoryNamedParameterJdbcTemplateImpl(dataSource());
    }

    @Bean
    public DataSource dataSource() {
        return new HikariDataSource(hikariConfig());
    }

    @Bean
    public HikariConfig hikariConfig() {
        HikariConfig config = new HikariConfig();
        config.setJdbcUrl(environment.getProperty("db.url"));
        config.setDriverClassName(environment.getProperty("db.driverClassName"));
        config.setUsername(environment.getProperty("db.user"));
        config.setPassword(environment.getProperty("db.password"));
        config.setMaximumPoolSize(environment.getProperty("db.hikari.max-pool-size", Integer.class));
        return config;
    }
}
