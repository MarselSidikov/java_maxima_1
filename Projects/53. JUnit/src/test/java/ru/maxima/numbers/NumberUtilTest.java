package ru.maxima.numbers;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;
import ru.maxima.numbers.NumberUtil;
import ru.maxima.providers.CompositeNumbersProvider;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.*;

/**
 * 17.01.2022
 * 53. JUnit
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@DisplayNameGeneration(value = DisplayNameGenerator.ReplaceUnderscores.class)
@DisplayName("NumberUtil is working when")
class NumberUtilTest {

    private NumberUtil numberUtil;

    @BeforeEach
    public void setUp() {
        this.numberUtil = new NumberUtil();
    }

    @DisplayName("gcd() is working, because")
    @Nested
    class ForGcd {
        @ParameterizedTest(name = "return {2} on a = {0} and b = {1}")
        @CsvSource(value = {"9, 12, 3", "18, 12, 6", "64, 48, 16"})
        public void gcd_on_numbers_return_correct_result(int a, int b, int expected) {
//            assertEquals(expected, numberUtil.gcd(a, b));
            assertThat(numberUtil.gcd(a, b), is(equalTo(expected)));
        }
    }

    @DisplayName("isPrime() is working, because")
    @Nested
    class ForIsPrime {
        @ParameterizedTest(name = "throws exception on {0}")
        @ValueSource(ints = {0, 1})
        public void on_problem_numbers_throws_exception(int problemNumber) {
            assertThrows(IllegalArgumentException.class, () -> numberUtil.isPrime(problemNumber));
        }

        @ParameterizedTest(name = "return <true> on {0}")
        @ValueSource(ints = {2, 3, 5, 41})
        public void on_prime_numbers_return_true(int primeNumber) {
            assertTrue(numberUtil.isPrime(primeNumber));
        }

        @ParameterizedTest(name = "return <false> on {0}")
        @ValueSource(ints = {121, 169})
        public void on_sqr_numbers_return_false(int sqrNumber) {
            assertFalse(numberUtil.isPrime(sqrNumber));
        }

        @ParameterizedTest(name = "return <false> on {0}")
        @ArgumentsSource(value = CompositeNumbersProvider.class)
        public void on_composite_number_return_false(int compositeNumber) {
            assertFalse(numberUtil.isPrime(compositeNumber));
        }
    }
}